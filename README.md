# Kotlin Android Utils

This is a collection of tiny but helpful utilities for Android development.
Currently the only one is [KPreferences](#kpreferences).

## KPreferences

This library gets rid of almost all boilerplate associated with preferences.

```kotlin
import me.smichel.android.KPreferences.Preference

// There's an optional third argument `mode: Int = Context.MODE_PRIVATE`
// For the one time you'll ever want to change it
class Config(context: Context, name: String) : Preferences(context, name) {

// You can also omit the 'name' argument to use the default shared preferences
// class Config(context: Context) : Preferences(context) {

// If you have a reference to your application context, a singleton is nice
// object Config : Preferences(applicationContext) {
    
    // Pass the resource id of the string key and the default value 
    var age by IntPreference(R.string.key_age, 35)
    
    // Or the string key itself
    val birthdateInMillis by LongPreference("birthday", 391780800000)

    // You can declare the type explicitly
    var fname: String by StringPreference(R.string.key_fname, "Jane")

    // Or allow it to be inferred
    var lname by StringPreference(R.string.key_lname, "Doe")
    
    // Delegates for all the standard preference types are provided
    var alive by BooleanPreference(R.string.key_alive, true)


    // Store more complex data types by proxy
    var person: Pair<String, Int>
        get() = Pair(fname + " " + lname, age)
        set(value) {
            fname = value.first.substringBefore(' ')
            lname = value.first.substringAfter(' ')
            age = value.third
        }
}
```

You can also store complex data by extending the `Preferences` class:

```kotlin
import android.content.Context
import me.smichel.android.KPreferences.Preferences
import org.json.JSONObject
import kotlin.reflect.KProperty

data class Person(val name: String, val yearsOld: Int) {
    // JSON keys are hard-coded for brevity, don't do that in production :P
    fun toJSON() = JSONObject().run {
        put("name", name)
        put("age", yearsOld)
        toString()
    }

    companion object {
        fun fromJSON(json: String): Person = JSONObject(json).run {
            val name = optString("name")
            val age = optInt("age")
            Person(name, age)
        }
    }
}

class ExtendedPreferences : Preferences {
    constructor(context: Context) : super(context)
    constructor(context: Context, name: String, mode: Int = Context.MODE_PRIVATE) : super(context, name, mode)

    inner class PersonPreference : Preference<Person> {
        constructor(resId: Int, default: Person) : super(resId, default)
        constructor(key: String, default: Person) : super(key, default)

        override fun getValue(thisRef: Any?, property: KProperty<*>): Person {
            return Person.fromJSON(prefs.getString(key, default.toJSON()))
        }

        override fun setValue(thisRef: Any?, property: KProperty<*>, value: Person) {
            prefs.edit().putString(key, value.toJSON()).apply()
        }
    }
}

class Config(context: Context) : ExtendedPreferences(context) {
    var person by PersonPreference("person", Person("Jane Doe", 35))
}
```
